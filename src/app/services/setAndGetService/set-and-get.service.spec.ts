import { TestBed } from '@angular/core/testing';

import { SetAndGetService } from './set-and-get.service';

describe('SetAndGetService', () => {
  let service: SetAndGetService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SetAndGetService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
