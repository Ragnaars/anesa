import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SetAndGetService {
  private valores : {[key : string] : any} = {};
  constructor() { }

  setValor(key:string, valor:string){
    this.valores[key] = valor;
  }

  getValor(key:string){
    return this.valores[key] || '';
  }
}
