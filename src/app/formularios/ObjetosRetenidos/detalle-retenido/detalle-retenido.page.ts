import { Component, inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule,Validators,FormGroup,FormBuilder} from '@angular/forms';
import { 
  IonContent, 
  IonHeader, 
  IonTitle, 
  IonToolbar, 
  IonIcon,
  IonButton,
  IonCard,
  IonButtons,
  IonBackButton,
  IonModal, IonRow, IonCol, IonSkeletonText, IonText } from '@ionic/angular/standalone';
 import { addIcons } from 'ionicons';
import { ActivatedRoute } from '@angular/router';
import { ObjRetenidosService } from 'src/app/services/obj-retenidos.service';
import { DatePipe } from '@angular/common';
import {jsPDF} from 'jspdf';


//componentes reciclables
import { StampComponent } from 'src/app/components/stamp/stamp.component';
import { SwipeButtonComponent } from 'src/app/components/swipe-button/swipe-button.component';
import { NavbarPage } from 'src/app/components/navbar/navbar.page';
import { SignaturePage } from 'src/app/components/signature/signature.page';
import { QrScannerPage } from 'src/app/components/qr-scanner/qr-scanner.page';
import { 
  camera, 
  qrCode, 
  create, 
  chevronForwardOutline, 
  download
} from 'ionicons/icons';

@Component({
  selector: 'app-detalle-retenido',
  templateUrl: './detalle-retenido.page.html',
  styleUrls: ['./detalle-retenido.page.scss'],
  standalone: true,
  imports: [
    StampComponent,
    IonText, 
    IonSkeletonText, 
    IonCol, 
    IonRow, 
    IonIcon, 
    IonContent, 
    IonHeader, 
    IonTitle, 
    IonToolbar, 
    CommonModule, 
    FormsModule,
    NavbarPage,
    IonButton,
    IonCard,
    IonBackButton,
    IonButtons,
    IonModal,
    QrScannerPage,
    SignaturePage,
    ReactiveFormsModule,
    SwipeButtonComponent
  ]
})


export class DetalleRetenidoPage implements OnInit {
  datePipe: DatePipe = new DatePipe('en-US');
  title = 'Detalle de Objeto Retenido'

  objs: any= [];
  obj!: any;


  //formulario
  formulario!:FormGroup;
  firma!:string;
  fecha!: string;

  constructor(
    private fb : FormBuilder,
    private rutaActiva : ActivatedRoute,
    private objService : ObjRetenidosService,
  ) { 
      this.formulario = this.fb.group({
      compania : ["",Validators.required],
      fechaVuelo : ["",Validators.required],
      horaVuelo : ["",Validators.required],
      vuelo : ["",Validators.required],
      destino : ["",Validators.required],
      puenteEmb : ["",Validators.required],
      objeto : ["",Validators.required],
      marca : ["",Validators.required],
      cantidad : ["",Validators.required],
      modelo : ["",Validators.required],
      serie : ["",Validators.required],
      asiento : ["",[Validators.required]],
      particular : ["",Validators.required],
      pax : ["",Validators.required],
      ticket : ["",Validators.required],
      funcionario : ["",Validators.required],
      numTel : ["",Validators.required],
      firma :["",Validators.required],
      fechaReg : [""],
      estadoReg : [""]
    })
  }

  ngOnInit() { 
    this.startAllIcons();
    this.rutaActiva.paramMap.subscribe(params => {
      const id = Number(params.get('id'));
      this.objService.getData().subscribe((data:any) => {
        this.objs = data.rows;
        this.obj = this.objs.find((obj:any) => obj.id === id);
        console.log(this.obj);
        this.formulario.controls['compania'].setValue(this.obj.compania);
        // this.formulario.controls['fechaVuelo'].setValue(this.obj.fechaVuelo);
        this.formulario.controls['horaVuelo'].setValue(this.obj.horaVuelo);
        this.formulario.controls['vuelo'].setValue(this.obj.vuelo);
        this.formulario.controls['destino'].setValue(this.obj.destino);
        this.formulario.controls['puenteEmb'].setValue(this.obj.puenteEmb);
        this.formulario.controls['objeto'].setValue(this.obj.objeto);
        this.formulario.controls['marca'].setValue(this.obj.marca);
        this.formulario.controls['cantidad'].setValue(this.obj.cantidad);
        this.formulario.controls['modelo'].setValue(this.obj.modelo);
        this.formulario.controls['serie'].setValue(this.obj.serie);
        this.formulario.controls['asiento'].setValue(this.obj.asiento);
        this.formulario.controls['particular'].setValue(this.obj.particular);
        this.formulario.controls['pax'].setValue(this.obj.pax);
        this.formulario.controls['ticket'].setValue(this.obj.ticket);
        this.formulario.controls['funcionario'].setValue(this.obj.funcionario);
        this.formulario.controls['numTel'].setValue(this.obj.numTel);
        this.firma = this.obj.firma;
        // this.formulario.controls['fechaReg'].setValue(this.obj.fechaReg);
        this.fecha = this.obj.fechaReg;
        this.formulario.controls['estadoReg'].setValue(this.obj.estadoReg);


      })
    })
  }

  exportPDF(){
    this.formulario.controls['fechaVuelo'].setValue(this.fecha);
    const formData = this.formulario.value;
    const doc = new jsPDF();

    //Titulo de documento
    doc.setFontSize(18);
    doc.text('Detalle de Objeto Retenido', 10, 10);

    //agregar dato del formulario ordenadamente
    let y = 30;
    for (const key in formData) {
      if (formData.hasOwnProperty(key)) {
        const element = formData[key];
        doc.text(`${key}: ${element}`, 10, y);
        y += 10;
      }
    }

    //guardar documento
    doc.save('DetalleObjetoRetenido.pdf');
  }

  startAllIcons(){
    addIcons({
      camera,
      create,
      qrCode,
      chevronForwardOutline,
      download
    })
  }




}
