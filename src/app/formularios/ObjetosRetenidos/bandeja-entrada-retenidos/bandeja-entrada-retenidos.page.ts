import { Component, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { 
  IonContent, 
  IonHeader, 
  IonTitle, 
  IonToolbar,
  IonButton,
  IonIcon,
  IonGrid,
  IonRow,
  IonCol,
  IonCard
 } from '@ionic/angular/standalone';
 import {
  camera, 
  qrCode,
  create,
} from 'ionicons/icons';

import { addIcons } from 'ionicons';

import { NavbarPage } from 'src/app/components/navbar/navbar.page';
import { ReactiveFormsModule,NgModel } from '@angular/forms';
import { FormBuilder} from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';


//Servicios
import { ObjRetenidosService } from 'src/app/services/obj-retenidos.service';
import { SetAndGetService } from 'src/app/services/setAndGetService/set-and-get.service';

//Componentes
import { DataTableComponent } from 'src/app/components/data-table/data-table.component';

@Component({
  selector: 'app-bandeja-entrada-retenidos',
  templateUrl: './bandeja-entrada-retenidos.page.html',
  styleUrls: ['./bandeja-entrada-retenidos.page.scss'],
  standalone: true,
  imports: [
    IonContent, 
    IonHeader, 
    IonTitle, 
    IonToolbar, 
    CommonModule, 
    FormsModule,
    NavbarPage,
    ReactiveFormsModule,
    IonButton,
    IonIcon,
    RouterLink,
    NgxDatatableModule,
    IonGrid,
    IonRow,
    IonCol,
    IonCard,
    DataTableComponent
  ]
})

export class BandejaEntradaRetenidosPage implements OnInit {

  title : string = "Bandeja de entrada de objetos retenidos";

  //formulario
  fechaVuelo : string = "";
  horaVuelo : string = "";
  destino : string = "";
  compania: string = "";
  vuelo : string = "";
  estado : string = "";

  constructor(
    private fb : FormBuilder, 
    private router : Router,
    private service : ObjRetenidosService,
    private sng : SetAndGetService
  ) { }

  ngOnInit() {
    this.startAllIcons();
  }

  startAllIcons(){
    addIcons({
      camera,
      create,
      qrCode
    })
  }

  onSubmit(){
    //reset values
    this.fechaVuelo = "";
    this.horaVuelo = "";
    this.destino = "";
    this.compania = "";
    this.vuelo = "";
    this.estado = "";
  }


}
