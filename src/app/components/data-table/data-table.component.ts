import { Component, OnInit, ViewChild ,Input, SimpleChanges} from '@angular/core';
import { ColumnMode, NgxDatatableModule, DatatableComponent } from '@swimlane/ngx-datatable';
import {IonGrid,IonRow,IonCol,IonCard, IonButton, IonIcon, IonInput, IonSearchbar } from '@ionic/angular/standalone';
import { ObjRetenidosService } from 'src/app/services/obj-retenidos.service';
import { NgIf } from '@angular/common';
import {Router} from '@angular/router';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  imports: [IonSearchbar, IonInput, 
    IonIcon, 
    IonButton, 
    NgxDatatableModule,
    IonGrid,
    IonRow,
    IonCol,
    IonCard,
    NgIf
  ],
  standalone: true,
  styleUrls: ['./data-table.component.scss'],
})
export class DataTableComponent  implements OnInit {
  @ViewChild(DatatableComponent) table!: DatatableComponent;
  @Input() compania: string = '';
  @Input() fechaVuelo: string = '';
  @Input() horaVuelo: string = '';
  @Input() vuelo: string = '';
  @Input() destino: string = '';
  @Input() estado!:any ;

//table
  ColumnMode = ColumnMode;
  loadingIndicator!:boolean;
  rows: any[] = [];
  temp: any[] = [];
  filteredRows: any[] = [];
  vari! : any;
  
  columns = [
    { prop: 'compania' }, 
    { prop: 'fechaVuelo' }, 
    { name: 'horaVuelo' }, 
    {prop: 'vuelo'},
    {name: 'destino'},
    {name: 'puenteEmb'},
    {name: 'objeto'},
    {name: 'marca'},
    {name: 'cantidad'},
    {name: 'modelo'},
    {name: 'serie'},
    {name: 'asiento'},
    {name: 'particular'},
    {name: 'pax'},
    {name: 'ticket'},
    {name: 'funcionario'},
    {name: 'numTel'},
    {name: 'firma'},
    {prop : 'fechaReg'},
    {prop : 'estado'}

  ];

  constructor(
    private service : ObjRetenidosService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getDataForTable();
  }
  
  getDataForTable() {
    this.service.getData().subscribe({
      next: data => {
        this.loadingIndicator = true;
        this.rows = [...data.rows];
        this.temp = [...data.rows]; // Guardar una copia de los datos originales
        this.filteredRows = [...data.rows];
        console.log(typeof this.rows[0].fechaVuelo);
        this.loadingIndicator = false;
      },
      error: err => {
        console.log(err);
      }
    });
  }

  updateFilter(event: any) {
    console.log(event);
    const val = event.target.value.toLowerCase();

    // Si el valor de búsqueda está vacío, restablece los datos originales
    if (!val) {
      this.rows = [...this.temp];
    } else {
      // Filtra los datos
      const temp = this.temp.filter((d: any) => {
        return d.compania.toLowerCase().indexOf(val) !== -1 ||
          d.fechaVuelo.toLowerCase().indexOf(val) !== -1 ||
          d.vuelo.toLowerCase().indexOf(val) !== -1 ||
          d.destino.toLowerCase().indexOf(val) !== -1 ||
          d.objeto.toLowerCase().indexOf(val) !== -1 ||
          d.pax.toLowerCase().indexOf(val) !== -1 ||
          d.ticket.toLowerCase().indexOf(val) !== -1;
      });

      // Actualiza las filas
      this.rows = temp;
    }
    
    // Siempre vuelve a la primera página
    this.table.offset = 0;
  }


  ngOnChanges(changes: SimpleChanges) {
    if (!this.loadingIndicator) { // Asegurarse de que los datos hayan sido cargados
      this.applyFilter();
    }
  }

  applyFilter() {
    let filtered = this.temp;

    if (this.compania) {
      filtered = filtered.filter(row => row.compania.toLowerCase().includes(this.compania.toLowerCase()));
    }
    if (this.fechaVuelo) {
      console.log(this.fechaVuelo);
      filtered = filtered.filter(row => row.fechaVuelo.toLowerCase().includes(this.fechaVuelo.toLowerCase()));
    }
    if (this.horaVuelo) {
      filtered = filtered.filter(row => row.horaVuelo.toLowerCase().includes(this.horaVuelo.toLowerCase()));
    }
    if (this.vuelo) {
      filtered = filtered.filter(row => row.vuelo.toLowerCase().includes(this.vuelo.toLowerCase()));
    }
    if (this.destino) {
      filtered = filtered.filter(row => row.destino.toLowerCase().includes(this.destino.toLowerCase()));
    }
    if (this.estado) {
      console.log(typeof this.estado, this.estado );
      if(this.estado === 'true'){
        this.estado = true;
      }else{
        this.estado = false;
      }
      console.log(typeof this.estado, this.estado );
      //string a boolean
      filtered = filtered.filter(row => row.estado == this.estado);
    }

    this.filteredRows = filtered;
    this.rows = filtered;

    if (this.table) {
      this.table.offset = 0; // Siempre vuelve a la primera página
    }
  }

  goToDetail(id:number){
    console.log(id);
    this.router.navigate(['detalle-retenido', id])
  }
 

  }
   