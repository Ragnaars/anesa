import { Component, OnInit, ViewChild} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonContent, IonHeader, IonTitle, IonToolbar, IonButton } from '@ionic/angular/standalone';
import {NgxScannerQrcodeComponent, NgxScannerQrcodeModule, NgxScannerQrcodeService} from 'ngx-scanner-qrcode';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-qr-scanner',
  templateUrl: './qr-scanner.page.html',
  styleUrls: ['./qr-scanner.page.scss'],
  standalone: true,
  imports: [
    IonButton, 
    IonContent, 
    IonHeader, 
    IonTitle, 
    IonToolbar, 
    CommonModule, 
    FormsModule,
    NgxScannerQrcodeModule]
})
export class QrScannerPage implements OnInit {
  @ViewChild('scanner') scanner!: NgxScannerQrcodeComponent;




  constructor(private qrService : NgxScannerQrcodeService) { 
   
  }

async ngOnInit() {
   try {
      const devices = await navigator.mediaDevices.enumerateDevices();
      console.log('Devices', devices);
      const videoDevices = devices.filter(device => device.kind === 'videoinput');
      const backCamera = videoDevices.find(device => device.label.toLowerCase().includes('back')) || videoDevices[0];

      if (backCamera) {
        console.log('Playing device', backCamera.deviceId);
        this.scanner.playDevice(backCamera.deviceId);
      }
    } catch (error) {
      console.error('Error accessing camera devices', error);
    }
  }

  
 
}
