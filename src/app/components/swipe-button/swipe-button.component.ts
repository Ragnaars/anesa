import { Component, ElementRef, Host, HostListener, OnInit, ViewChild, NgZone} from '@angular/core';
import {
  IonRow, 
  IonCol, 
  IonText, 
  IonFabButton, 
  IonButton, 
  IonIcon } from '@ionic/angular/standalone';
import {Router} from '@angular/router';
import {GestureController,ToastController} from '@ionic/angular';
import { addIcons } from 'ionicons';
import {
  chevronForward,
  chevronForwardOutline} from 'ionicons/icons';
@Component({
  selector: 'app-swipe-button',
  templateUrl: './swipe-button.component.html',
  styleUrls: ['./swipe-button.component.scss'],
  standalone: true,
  imports: [IonIcon, IonButton, IonFabButton, 
    IonText, 
    IonCol, 
    IonRow]
})


export class SwipeButtonComponent  implements OnInit {
  @ViewChild('swipeButton', {read : ElementRef}) swipeButton!: ElementRef;
  text : string = "Confirmar entrega";
  color = '312c'


  //variables importantes
  swipeInProgress : boolean = false;
  colWidth!: number;
  translateX!: number ;

  swipeGesture!:any;

  constructor(
    private ngZone : NgZone,
    private router:Router,
    private gestureCtrl:GestureController,
    private toastCtrl:ToastController
  ) { }

  ngOnInit() {
    this.startAllIcons();
    
  }
  
  ngAfterViewInit(): void {
    this.createSwipeGesture();
  }

  private createSwipeGesture(){
    this.swipeGesture = this.gestureCtrl.create({
      el : this.swipeButton.nativeElement,
      threshold : 10,
      gestureName: 'swipe', 
      onStart : () => {
        this.swipeInProgress = true;
      },
      onMove : (detail) =>{ 
        if(this.swipeInProgress && detail.deltaX > 0){
          const deltaX = detail.deltaX;
          const colWidth = this.swipeButton.nativeElement.parentElement.clientWidth;
          this.colWidth = colWidth - (8/100 * colWidth);
          this.translateX = Math.min(deltaX, this.colWidth);
          this.swipeButton.nativeElement.style.transform = `translateX(${this.translateX}px)`;
        }

      },
      onEnd : (detail) => {
        if(this.translateX >= this.colWidth){

          this.showToast();
          this.ngZone.run(() => {
            this.router.navigate(['/bandeja-entrada-retenidos']);
          });
        }
        this.swipeInProgress = false;
        this.swipeButton.nativeElement.style.transform = `translateX(0px)`;
      }
    });
    this.swipeGesture.enable(true);
  }

  async showToast(){
    const toast = await this.toastCtrl.create({
      message: 'Confirmando',
      color: 'warning',
      position: 'middle',
      duration: 3000
    });
    toast.present();

  }
  
  startAllIcons(){
    addIcons({
      chevronForward,
      chevronForwardOutline,
    })
  }


}
